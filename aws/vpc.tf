module "vpc" {
  source = "cloudposse/vpc/aws"
  # Cloud Posse recommends pinning every module to a specific version
  version     = "1.1.0"
  namespace = var.namespace
  stage     = var.environment
  name      = var.project_name

  ipv4_primary_cidr_block = "10.0.0.0/16"

  assign_generated_ipv6_cidr_block = false
}

module "dynamic_subnets" {
  source = "cloudposse/dynamic-subnets/aws"
  # Cloud Posse recommends pinning every module to a specific version
  version     = "2.0.2"
  namespace          = var.namespace
  stage              = var.environment
  name               = var.project_name
  availability_zones = ["eu-west-1a","eu-west-1b","eu-west-1c"]
  vpc_id             = module.vpc.vpc_id
  igw_id             = [module.vpc.igw_id]
  ipv4_cidr_block         = ["10.0.0.0/16"]
}