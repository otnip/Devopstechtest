variable "aws_profile" {
  type = string
}
variable "aws_region" {
  type = string
}
variable "shared_credentials_file" {
  type = string
}

variable "environment" {
  type        = string
  description = "environment name"
}

variable "tags" {
  type        = map(any)
  default     = {}
  description = "A map of tags to assign to the resource."
}

variable "project_name" {
  type        = string
  default     = "sportbuff"
  description = "The name of the project"
}

variable "namespace" {
  type = string
  default = "xy"
}

# activemq
variable "activemq_service_name" {
  type = string
  default = "activemq"
}
variable "activemq_image"  {
  type = string
  default = "rmohr/activemq:5.15.9-alpine"
}
variable "activemq_port1"  {
  type = number
  default = 8161
}
variable "activemq_port2"  {
  type = number
  default = 61613
}
variable "activemq_cpu"  {
  type = number
  default = 256
}
variable "activemq_memory"  {
  type = number
  default = 512
}

# dashboard
variable "dashboard_service_name" {
  type = string
  default = "dashboard"
}
variable "dashboard_image"  {
  type = string
  default = "opistola/sbdashboard"
}
variable "dashboard_port"  {
  type = number
  default = 8080
}
variable "dashboard_cpu"  {
  type = number
  default = 256
}
variable "dashboard_memory"  {
  type = number
  default = 512
}

# server
variable "server_service_name" {
  type = string
  default = "server"
}
variable "server_image"  {
  type = string
  default = "opistola/sbserver"
}
variable "server_port"  {
  type = number
  default = 8081
}
variable "server_port_internal"  {
  type = number
  default = 8080
}
variable "server_cpu"  {
  type = number
  default = 256
}
variable "server_memory"  {
  type = number
  default = 512
}

variable "identifier"  {
  type = string
}

variable "db_name"  {
  type = string
}

variable "username"  {
  type = string
}

variable "password"  {
  type = string
}

variable "port"  {
  type = string
}