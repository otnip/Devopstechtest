output "alb_dns_name" {
  value = aws_alb.main.dns_name
}
output "internal_alb_dns_name" {
  value = aws_lb.internal.dns_name
}
output "mysql_server" {
  value = module.mysql.db_instance_address
}
output "db_password" {
  sensitive = true
  value = module.mysql.db_instance_password
}
output "db_username" {
  sensitive = true
  value = module.mysql.db_instance_username
}
output "redis_server" {
  value = module.redis.elasticache_replication_group_primary_endpoint_address
}

