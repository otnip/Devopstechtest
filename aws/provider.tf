terraform {
  #required_version = "~> 0.13.0"
  backend "s3" {
    profile        = "opistolaaws"
    bucket         = "sportbuff-dev-s3b-m5zo4kdc" //Get the bucket name from s3_backend_bootstrap output.
    dynamodb_table = "sportbuff-dev-ddbtable"
    key            = "sportbuff-dev.tfstate"
    region         = "eu-west-1"
    encrypt        = true
  }
}

provider "aws" {
  profile                 = var.aws_profile
  region                  = var.aws_region
  shared_credentials_files = [var.shared_credentials_file]
}

