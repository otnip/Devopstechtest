# public loadbalancer
resource "aws_alb" "main" {
  name            = "${var.project_name}-${var.environment}-load-balancer"
  subnets         = module.dynamic_subnets.public_subnet_ids
  security_groups = [aws_security_group.alb_sg.id]
}

resource "aws_alb_target_group" "dashboard" {
  name        = "${var.project_name}-${var.environment}-dashboard-tg"
  port        = 8080
  protocol    = "HTTP"
  vpc_id      = module.vpc.vpc_id
  target_type = "ip"

  health_check {
    healthy_threshold   = "3"
    interval            = "30"
    protocol            = "HTTP"
    matcher             = "200-499"
    timeout             = "3"
    path                = "/buffs"
    unhealthy_threshold = "2"
  }
}

resource "aws_alb_target_group" "server" {
  name        = "${var.project_name}-${var.environment}-server-tg"
  port        = 8080
  protocol    = "HTTP"
  vpc_id      = module.vpc.vpc_id
  target_type = "ip"

  health_check {
    healthy_threshold   = "3"
    interval            = "30"
    protocol            = "HTTP"
    matcher             = "200-499"
    timeout             = "3"
    path                = "/buff"
    unhealthy_threshold = "2"
  }
}

# Redirect all traffic from the ALB to the target group
resource "aws_alb_listener" "front_end" {
  load_balancer_arn = aws_alb.main.id
  port              = 80
  protocol          = "HTTP"

  default_action {
    target_group_arn = aws_alb_target_group.dashboard.id
    type             = "forward"
  }
}

/*
resource "aws_lb_listener_rule" "dashboard" {
  listener_arn = aws_alb_listener.front_end.arn
  priority     = 101

  action {
    type             = "forward"
    target_group_arn = aws_alb_target_group.dashboard.arn
  }

  condition {
    path_pattern {
      values = ["/buffs"]
    }
  }
}
*/
resource "aws_lb_listener_rule" "server" {
  listener_arn = aws_alb_listener.front_end.arn
  priority     = 102

  action {
    type             = "forward"
    target_group_arn = aws_alb_target_group.server.arn
  }

  condition {
    path_pattern {
      values = ["/buffs"]
    }
  }
}

#########
# network nlb
#########

resource "aws_lb" "internal" {
  name    = "${var.project_name}-${var.environment}-internal-nlb"
  subnets = module.dynamic_subnets.private_subnet_ids
  #security_groups    = [aws_security_group.int_alb_sg.id]
  load_balancer_type = "network"
  internal           = true
}

resource "aws_alb_target_group" "activemq_8161" {
  name        = "${var.project_name}-${var.environment}-activemq-8161-tg"
  port        = 8161
  protocol    = "TCP"
  vpc_id      = module.vpc.vpc_id
  target_type = "ip"

  /*
  health_check {
    healthy_threshold   = "3"
    interval            = "30"
    protocol            = "HTTP"
    matcher             = "200"
    timeout             = "3"
    path                = "/"
    unhealthy_threshold = "2"
  }
  */
}

resource "aws_alb_target_group" "activemq_61613" {
  name        = "${var.project_name}-${var.environment}-activemq-61613-tg"
  port        = 61613
  protocol    = "TCP"
  vpc_id      = module.vpc.vpc_id
  target_type = "ip"

  /*
  health_check {
    healthy_threshold   = "3"
    interval            = "30"
    protocol            = "HTTP"
    matcher             = "200"
    timeout             = "3"
    path                = "/"
    unhealthy_threshold = "2"
  }
  */
}

## Redirect all traffic from the ALB to the target group
resource "aws_alb_listener" "activemq_8161" {
  load_balancer_arn = aws_lb.internal.arn
  port              = 8161
  protocol          = "TCP"

  default_action {
    target_group_arn = aws_alb_target_group.activemq_8161.id
    type             = "forward"
  }
}

resource "aws_alb_listener" "activemq_61613" {
  load_balancer_arn = aws_lb.internal.arn
  port              = 61613
  protocol          = "TCP"

  default_action {
    target_group_arn = aws_alb_target_group.activemq_61613.id
    type             = "forward"
  }
}