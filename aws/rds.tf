module "mysql" {
  source  = "terraform-aws-modules/rds/aws"
  version = "4.4.0"

  identifier = var.identifier

  iam_database_authentication_enabled = false

  engine               = "mysql"
  engine_version       = "8.0.27"
  family               = "mysql8.0" # DB parameter group
  major_engine_version = "8.0"      # DB option group
  instance_class       = "db.t2.small"
  allocated_storage    = 5

  db_name  = var.db_name
  username = var.username
  password = var.password
  create_random_password = false
  port                   = var.port

  vpc_security_group_ids = [aws_security_group.int_alb_sg.id]

  tags = {
    Owner       = var.project_name
    Environment = var.environment
  }

  # DB subnet group
  create_db_subnet_group = true
  subnet_ids             = module.dynamic_subnets.private_subnet_ids

  # Database Deletion Protection
  deletion_protection = false

  backup_retention_period = 0
  skip_final_snapshot     = true

  parameters = [
    {
      name  = "character_set_client"
      value = "utf8mb4"
    },
    {
      name  = "character_set_server"
      value = "utf8mb4"
    }
  ]
}