[
  {
    "name": "${server_service_name}",
    "image": "${server_image}",
    "cpu": ${server_cpu},
    "memory": ${server_memory},
    "networkMode": "awsvpc",
    "logConfiguration": {
        "logDriver": "awslogs",
        "options": {
          "awslogs-group": "/ecs/${namespace}-${environment}-${project_name}-${server_service_name}",
          "awslogs-region": "${region}",
          "awslogs-stream-prefix": "ecs"
        }
    },
    "secrets": [
    {
        "name": "DBNAME",
        "valueFrom": "arn:aws:ssm:eu-west-1:745065723172:parameter/sb/dbname"
    },
    {
        "name": "DBHOST",
        "valueFrom": "arn:aws:ssm:eu-west-1:745065723172:parameter/sb/dbhost"
    },
    {
        "name": "DBPORT",
        "valueFrom": "arn:aws:ssm:eu-west-1:745065723172:parameter/sb/dbport"
    },
    {
        "name": "DBUSER",
        "valueFrom": "arn:aws:ssm:eu-west-1:745065723172:parameter/sb/dbuser"
    },
    {
        "name": "DBPASS",
        "valueFrom": "arn:aws:ssm:eu-west-1:745065723172:parameter/sb/dbpass"
    },
    {
        "name": "REDISHOSTS",
        "valueFrom": "arn:aws:ssm:eu-west-1:745065723172:parameter/sb/redishosts"
    },
    {
        "name": "MQHOST",
        "valueFrom": "arn:aws:ssm:eu-west-1:745065723172:parameter/sb/mqhost"
    },
    {
        "name": "MQPORT",
        "valueFrom": "arn:aws:ssm:eu-west-1:745065723172:parameter/sb/mqport"
    },
    {
        "name": "MQUSERNAME",
        "valueFrom": "arn:aws:ssm:eu-west-1:745065723172:parameter/sb/mqusername"
    },
    {
        "name": "MQPASSWORD",
        "valueFrom": "arn:aws:ssm:eu-west-1:745065723172:parameter/sb/mqpassword"
    }

    ],
    "portMappings": [
      {
        "containerPort": ${server_port},
        "hostPort": ${server_port}
      }
    ]
  }
]