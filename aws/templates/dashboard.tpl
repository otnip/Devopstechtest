[
  {
    "name": "${dashboard_service_name}",
    "image": "${dashboard_image}",
    "cpu": ${dashboard_cpu},
    "memory": ${dashboard_memory},
    "networkMode": "awsvpc",
    "logConfiguration": {
        "logDriver": "awslogs",
        "options": {
          "awslogs-group": "/ecs/${namespace}-${environment}-${project_name}-${dashboard_service_name}",
          "awslogs-region": "${region}",
          "awslogs-stream-prefix": "ecs"
        }
    },
    "secrets": [
        {
            "name": "MQHOST",
            "valueFrom": "arn:aws:ssm:eu-west-1:745065723172:parameter/sb/mqhost"
        },
        {
            "name": "MQPORT",
            "valueFrom": "arn:aws:ssm:eu-west-1:745065723172:parameter/sb/mqport"
        },
        {
            "name": "MQUSERNAME",
            "valueFrom": "arn:aws:ssm:eu-west-1:745065723172:parameter/sb/mqusername"
        },
        {
            "name": "MQPASSWORD",
            "valueFrom": "arn:aws:ssm:eu-west-1:745065723172:parameter/sb/mqpassword"
        }
    ],
    "portMappings": [
      {
        "containerPort": ${dashboard_port},
        "hostPort": ${dashboard_port}
      }
    ]
  }
]