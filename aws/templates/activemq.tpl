[
  {
    "name": "${activemq_service_name}",
    "image": "${activemq_image}",
    "cpu": ${activemq_cpu},
    "memory": ${activemq_memory},
    "networkMode": "awsvpc",
    "logConfiguration": {
        "logDriver": "awslogs",
        "options": {
          "awslogs-group": "/ecs/${namespace}-${environment}-${project_name}-${activemq_service_name}",
          "awslogs-region": "${region}",
          "awslogs-stream-prefix": "ecs"
        }
    },
    "portMappings": [
      {
        "containerPort": ${activemq_port1},
        "hostPort": ${activemq_port1}
      },
      {
          "containerPort": ${activemq_port2},
          "hostPort": ${activemq_port2}
        }
    ]
  }
]