# Set up CloudWatch group and log stream and retain logs for 30 days
resource "aws_cloudwatch_log_group" "log_group" {
  name              = "/ecs/${var.namespace}-${var.environment}-${var.project_name}"
  retention_in_days = 30

  tags = {
    Name = "log-group-${var.namespace}-${var.environment}-${var.project_name}"
  }
}

resource "aws_cloudwatch_log_stream" "cb_log_stream" {
  name           = "log-stream-${var.namespace}-${var.environment}-${var.project_name}"
  log_group_name = aws_cloudwatch_log_group.log_group.name
}

# Set up CloudWatch group and log stream and retain logs for 30 days
resource "aws_cloudwatch_log_group" "log_group_activemq" {
  name              = "/ecs/${var.namespace}-${var.environment}-${var.project_name}-${var.activemq_service_name}"
  retention_in_days = 30

  tags = {
    Name = "log-group-${var.namespace}-${var.environment}-${var.project_name}-${var.activemq_service_name}"
  }
}

resource "aws_cloudwatch_log_stream" "log_stream_activemq" {
  name           = "log-stream-${var.namespace}-${var.environment}-${var.project_name}"
  log_group_name = aws_cloudwatch_log_group.log_group_activemq.name
}

# Set up CloudWatch group and log stream and retain logs for 30 days DASHBOARD
resource "aws_cloudwatch_log_group" "log_group_dashboard" {
  name              = "/ecs/${var.namespace}-${var.environment}-${var.project_name}-${var.dashboard_service_name}"
  retention_in_days = 30

  tags = {
    Name = "log-group-${var.namespace}-${var.environment}-${var.project_name}-${var.dashboard_service_name}"
  }
}

resource "aws_cloudwatch_log_stream" "log_stream_dashboard" {
  name           = "log-stream-${var.namespace}-${var.environment}-${var.project_name}"
  log_group_name = aws_cloudwatch_log_group.log_group_dashboard.name
}

# Set up CloudWatch group and log stream and retain logs for 30 days SERVER
resource "aws_cloudwatch_log_group" "log_group_server" {
  name              = "/ecs/${var.namespace}-${var.environment}-${var.project_name}-${var.server_service_name}"
  retention_in_days = 30

  tags = {
    Name = "log-group-${var.namespace}-${var.environment}-${var.project_name}-${var.server_service_name}"
  }
}

resource "aws_cloudwatch_log_stream" "log_stream_server" {
  name           = "log-stream-${var.namespace}-${var.environment}-${var.project_name}"
  log_group_name = aws_cloudwatch_log_group.log_group_server.name
}
