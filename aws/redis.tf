

module "redis" {
  source  = "umotif-public/elasticache-redis/aws"
  version = "3.1.2"

  name_prefix        = "${var.environment}-${var.project_name}-redisdb"
  num_cache_clusters = 2
  node_type          = "cache.t3.small"

  cluster_mode_enabled    = true
  replicas_per_node_group = 1
  num_node_groups         = 2
  automatic_failover_enabled = true

  engine_version            = "6.x"
  port                      = 6379

  at_rest_encryption_enabled = false
  transit_encryption_enabled = false
  #auth_token                 = "1234567890asdfghjkl"

  apply_immediately = true
  family            = "redis6.x"
  description       = "Test elasticache redis."

  subnet_ids = module.dynamic_subnets.private_subnet_ids
  vpc_id     = module.vpc.vpc_id

  allowed_security_groups = [aws_security_group.int_alb_sg.id]

  ingress_cidr_blocks = ["0.0.0.0/0"]

  parameter = [
    {
      name  = "repl-backlog-size"
      value = "16384"
    }
  ]

  tags = {
    Project = var.project_name
    Environment = var.environment
  }
}