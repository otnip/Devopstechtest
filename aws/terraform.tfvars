aws_profile             = "THEPROFILE"
aws_region              = "eu-west-1"
shared_credentials_file = "~/.aws/credentials"
environment             = "dev"

namespace    = "xy"
project_name = "sportbuff"

# rds mysql
identifier = "buffup"
db_name    = "buffup"
username   = "buffup"
password   = "THEMYSQLPASSWORD"
port       = "3306"

# activemq
activemq_service_name = "activemq"
activemq_image        = "opistola/activemq:0.1"
activemq_port1        = 8161
activemq_port2        = 61613
activemq_cpu          = 256
activemq_memory       = 512

# dashboard
dashboard_service_name = "dashboard"
dashboard_image        = "opistola/sbdashboard:0.1"
dashboard_port         = 8080
dashboard_cpu          = 256
dashboard_memory       = 512

# server
server_service_name = "server"
server_image        = "opistola/sbserver:0.1"
server_port         = 8080
server_cpu          = 256
server_memory       = 512