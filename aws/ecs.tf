resource "aws_ecs_cluster" "ecs_cluster" {
  name = "${var.namespace}-${var.environment}-${var.project_name}"
  tags = {
    Name = "${var.namespace}-${var.environment}-${var.project_name}"
  }
  setting {
    name  = "containerInsights"
    value = "enabled"
  }
}

data "template_file" "dashboard" {
  template = file("./templates/dashboard.tpl")
  vars = {
    namespace              = var.namespace
    project_name           = var.project_name
    environment            = var.environment
    dashboard_service_name = var.dashboard_service_name
    dashboard_image        = var.dashboard_image
    dashboard_port         = var.dashboard_port
    dashboard_cpu          = var.dashboard_cpu
    dashboard_memory       = var.dashboard_memory
    region                 = var.aws_region
  }
}

data "template_file" "server" {
  template = file("./templates/server.tpl")
  vars = {
    namespace           = var.namespace
    project_name        = var.project_name
    environment         = var.environment
    server_service_name = var.server_service_name
    server_image        = var.server_image
    server_port         = var.server_port
    server_port_internal         = var.server_port_internal
    server_cpu          = var.server_cpu
    server_memory       = var.server_memory
    region              = var.aws_region
  }
}

data "template_file" "activemq" {
  template = file("./templates/activemq.tpl")

  vars = {
    namespace             = var.namespace
    project_name          = var.project_name
    environment           = var.environment
    activemq_service_name = var.activemq_service_name
    activemq_image        = var.activemq_image
    activemq_port1        = var.activemq_port1
    activemq_port2        = var.activemq_port2
    activemq_cpu          = var.activemq_cpu
    activemq_memory       = var.activemq_memory
    region                = var.aws_region
  }
}

resource "aws_ecs_task_definition" "dashboard" {
  family                   = "${var.project_name}-${var.environment}-dashboard-task"
  execution_role_arn       = aws_iam_role.ecs_task_execution_role.arn
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = 256
  memory                   = 512
  container_definitions    = data.template_file.dashboard.rendered
}

resource "aws_ecs_task_definition" "server" {
  family                   = "${var.project_name}-${var.environment}-server-task"
  execution_role_arn       = aws_iam_role.ecs_task_execution_role.arn
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = 256
  memory                   = 512
  container_definitions    = data.template_file.server.rendered
}

resource "aws_ecs_task_definition" "activemq" {
  family                   = "${var.project_name}-${var.environment}-activemq-task"
  execution_role_arn       = aws_iam_role.ecs_task_execution_role.arn
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = 256
  memory                   = 512
  container_definitions    = data.template_file.activemq.rendered
}

resource "aws_ecs_service" "dashboard" {
  name            = "dashboard-service"
  cluster         = aws_ecs_cluster.ecs_cluster.id
  task_definition = aws_ecs_task_definition.dashboard.arn
  desired_count   = 1
  launch_type     = "FARGATE"

  network_configuration {
    security_groups  = [aws_security_group.alb_sg.id]
    subnets          = module.dynamic_subnets.public_subnet_ids
    assign_public_ip = true
  }

  load_balancer {
    target_group_arn = aws_alb_target_group.dashboard.id
    container_name   = "dashboard"
    container_port   = 8080
  }

  depends_on = [aws_alb_listener.front_end, aws_iam_role_policy_attachment.ecs-task-execution-role-policy-attachment]
}

resource "aws_ecs_service" "server" {
  name            = "server-service"
  cluster         = aws_ecs_cluster.ecs_cluster.id
  task_definition = aws_ecs_task_definition.server.arn
  desired_count   = 1
  launch_type     = "FARGATE"

  network_configuration {
    security_groups  = [aws_security_group.alb_sg.id]
    subnets          = module.dynamic_subnets.public_subnet_ids
    assign_public_ip = true
  }

  load_balancer {
    target_group_arn = aws_alb_target_group.server.id
    container_name   = "server"
    container_port   = 8080
  }

  depends_on = [aws_alb_listener.front_end, aws_iam_role_policy_attachment.ecs-task-execution-role-policy-attachment]
}

resource "aws_ecs_service" "activemq" {
  name            = "activemq-service"
  cluster         = aws_ecs_cluster.ecs_cluster.id
  task_definition = aws_ecs_task_definition.activemq.arn
  desired_count   = 1
  launch_type     = "FARGATE"

  network_configuration {
    security_groups  = [aws_security_group.int_alb_sg.id]
    subnets          = module.dynamic_subnets.private_subnet_ids
    assign_public_ip = false
  }

  load_balancer {
    target_group_arn = aws_alb_target_group.activemq_8161.id
    container_name   = "activemq"
    container_port   = 8161
  }
  load_balancer {
    target_group_arn = aws_alb_target_group.activemq_61613.id
    container_name   = "activemq"
    container_port   = 61613
  }

  depends_on = [aws_alb_listener.activemq_8161, aws_iam_role_policy_attachment.ecs-task-execution-role-policy-attachment]
}