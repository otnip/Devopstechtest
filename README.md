# Sergio Pinto proposal

## About decisions i made

I started by trying to use the modules provided but soon I realize I was loosing more time than actually saving time 
so i decided to start fresh. Still i used the cloudposs vpc and dynamic subnets.

Since the application is already working with containers I decided to go with AWS ECS as the base 
for running the application and use as much i can, services from AWS to substitute some 
containers. For me this is almost mandatory, use as much software as a service as possible this allows 
us to focus more on the application itself and not on the support services, not mention access to great 
features that allow us the scale, backup and monitor this aws provided services. It comes with a cost, but 
it will pay for itself in the future. Some good practices here that i would follow is to use always at least 
3 availability zones and 3 replicas of each service and application running each on one availability zone. 

My first intention was to only use ECS, for the dashboard and server application,
but because activemq is not something i am conformable with, I decided to include 
the container as part of the ECS service. AWS has an activemq service that would be a great substitute, but it would 
take me more time and effort to make it work with the application.
Having to include the activemq leave me with having to implement an internal network load balancer with 
2 listeners and 2 target groups (because of the 2 ports) just to protect this service from the internet and make it scalable. 
To scale we just need to increase the number of task definitions on the service

For the redis cluster i used aws elastic cache redis working as a cluster. Working with this AWS service we just need to 
the main endpoint and the service handle the rest.

For the mysql I used RDS. It was not implemented as a cluster but for prod environment this follows the same principle as 
redis.

I tried to make it as much scalable as possible, but I am not aware which part of it all systems needs to have more 
attention or less attention so i tried to make all the components scalable.

Application logs are handled with AWS cloudwatch.

## About Architecture

The architecture is based on a public Application load balancer linked with 3 public subnets and with a target group
 attached to the ECS service-dashboard and service-server application. These are the only services that are exposed to
 the internet. For connectivity between the service-dashboard and service-server to the support services the connection 
is made using the 3 private subnets. From service-dashboard and service-server to activemq container, an internal loadbalancer 
 is used with 2 target groups and 2 listeners (because of the 2 activemq ports). From service-dashboard and service-server 
redis cluster and mysql db the connection is made with the private subnets. None of the supporting services are exposed to the 
internet and are only reachable when the communication initiates from the vpc.

The public application load balancer have a sg attached which is accepting all traffic from the internet to port 80.
On the hosts/services internal a security group is attached which allows any communication from the same vpc cidr.

I used my dockerhub account to upload the containers for speedup things but using AWS Ecr seems the right decision here.

## What is missing or not well done from the test and time to make it good

I have used System Manager parameter store to handle secrets and key values to use on the applications but 
i have not used any parameter with encryption. I should have used at least on the ones that store secrets like 
db password and mq password. Some other values may or may not be candidates to use encryption like db name, db user or 
mq username. Also, I used parameters mostly for the ECS task definitions and not as part of the terraform scripts.
On the terraform, these values should be imported as data resources to be injected on the modules. Other solutions 
may be to use Hashicorp Vault for the secrets. Should take 1-3 hours.

The RDS module used in the exercise have flags that enable backup schedules and data retention. We just need to enable
them. To restore from a backup It's easy to be done from the AWS console. There are also third party software that make this
 really easy like n2ws. the snapshot and third party software usually do image backup that allow to restore a db. This works great, 
but it takes time which is not always acceptable. I would use this feature as a last resort and prepare the deployments upgrade/downgrade 
using feature flags on the side of the code and use a migrations' library to handling schema changes on the db like liquidbase. To study and implement 
a strategy should take at least 1 week.

Auto-Scaling was not implemented on any of the services and application. Autoscaling the ECS services should take 2 hours
Alerts were not implemented also. Make alerts for the services already monitored - 1 day

The monitoring implemented was the ones that are given from AWS.

Public access and internal access protection are a bit wide. I would review all the architecture based on the principle of least privilege. 
All hosts and services should be only allow to communicate with the necessary parties, no more no less. Should take 2-4 hours

Not implemented but mandatory also is to protect data in transit and data at rest. So, all storage should be encrypted
and all communications should use TLS. Should take 1 day

Containers should run with a specific user with only the necessary permissions to run the app and all containers 
should be scanned for vulnerabilities and fixed accordingly. Implement and check should take 2-4 hours, container vulnerability check i am not sure, it depends on the amount of fixes to correct or assume the risk.

## how to run

1- Go into folder terraform/aws

2- check terraform.tfvars file and update accordingly

3- Create on the parameter store the following entries:

```
/sb/dbhost
/sb/dbname
/sb/dbpass
/sb/dbport
/sb/dbuser
/sb/mqhost
/sb/mqpassword
/sb/mqport
/sb/mqusername
/sb/redishosts
```

This must be populated for the containers to know how to communicate with services. Some of the values will be 
exposed on the end of terraform execution.

4- Run:

```
make init
make deploy
```

5- Check the output values some will be needed to be used by the containers. It should look like this:

```
alb_dns_name = "sportbuff-dev-load-balancer-124571936.eu-west-1.elb.amazonaws.com"
db_password = <sensitive>
db_username = <sensitive>
internal_alb_dns_name = "sportbuff-dev-internal-nlb-d839a38716177ed9.elb.eu-west-1.amazonaws.com"
mysql_server = "buffup.ceocuzygzha1.eu-west-1.rds.amazonaws.com"
redis_server = "dev-sportbuff-redisdb-redis.xmlvjt.clustercfg.euw1.cache.amazonaws.com"
```

6- Update the parameters on system manager

7- Go into the rds and put the desired password (not sure if this is a bug or not but for some reason it is not assumed) and apply changes immediately.

8- Check ecs services and target groups health checks and if all ok make the tests by using the public dns name of the load balancer.

## Last thing

I took around 20 hours to complete this



